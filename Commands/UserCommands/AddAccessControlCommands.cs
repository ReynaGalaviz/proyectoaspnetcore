using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ProyectoAspNetCore.Domain;
using ProyectoAspNetCore.RabbitMq;

namespace ProyectoAspNetCore.Commands.UserCommands
{

        public class AddAccessControlCommand : IRequest
    {
        public AccessType AccessType { get; set; }
    }

   public class AddAccessControlCommandHandler : IRequestHandler<AddAccessControlCommand>
    {
        private readonly IRabbitMqPublish rabbitMqPublish;

        public AddAccessControlCommandHandler(IRabbitMqPublish rabbitMqPublish)
        {
            this.rabbitMqPublish = rabbitMqPublish;
        }

        public Task<Unit> Handle(AddAccessControlCommand request, CancellationToken cancellationToken)
        {
            // 2.- MITOTEAR a RabbitMq
            // var message = new MessageClean("OpenServo");
            var message = new MoveMotor(3, 90);
            var result = rabbitMqPublish.Publish<MoveMotor>(message, "DataFromAspNetCore");

            if(result==false)
                throw new Exception("No se ha notificado a Arduino");


            return Task.FromResult<Unit>(new Unit());
        }
    }
}