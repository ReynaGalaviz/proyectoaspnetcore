using System;
using System.Text;
using System.Text.Json;
using RabbitMQ.Client;

namespace ProyectoAspNetCore.RabbitMq
{
    public class IRabbitMqPublish
    {
        bool Publish<T>(T message, string routingkey);
    }
    public class RabbitMqPublish : IRabbitMqPublish
    {
        private readonly IRabbitMqSettings rabbitMqSettings;

        public RabbitMqPublish(IRabbitMqSettings rabbitMqSettings)
        {
            this.rabbitMqSettings = rabbitMqSettings;
            this.rabbitMqSettings.QueueName+= "_"+Guid.NewGuid();
        }

        public bool Publish<T>(T message, string routingkey)
        {
            try
            {
                string json = JsonSerializer.Serialize(message).Replace(" ", "");

                var factory = new ConnectionFactory
                {
                    Uri = rabbitMqSettings.Uri
                };
                var connection = factory.CreateConnection();
                var channel = connection.CreateModel();

                var queueName = rabbitMqSettings.QueueName;
                 //Declaramos la cola a la que nos conectaremos para enviar la información al RoutingKey amq.topic
                channel.QueueDeclare(queueName, durable: true, exclusive: false, autoDelete: true, arguments: null);
                var body = Encoding.UTF8.GetBytes(json);

                return true;
            }
            catch (System.Exception ex)
            {
                return false;
            }
        }

        void metodo<T>()
        {
            int numero;
            string palabra;
            bool pregunta;

            T mivariable;

            this.metodo<int>();

            this.metodo<string>();

            this.metodo<bool>();

            this.metodo<RabbitMqSettings>();


        }
    }
}